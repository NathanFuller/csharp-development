﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        private static bool isName = false;

        private static String getYourName()
        {
            Console.WriteLine("What's your name?\n");
            string yourName = Console.ReadLine();
            return yourName;
        }

        private static String getFavoriteLanguage(string yourName)
        {
            Console.WriteLine("\nWhat's your favorite programming language, " + yourName + "?\n");
            string faveLang = Console.ReadLine();
            return faveLang;
        }

        private static bool isCSharp(string faveLang)
        {
            if (faveLang.Equals("C#") || faveLang.Equals("C Sharp"))
                return true;
            else
                return false;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello there!");
            string yourName = getYourName();
            while(!isName)
            {
                Console.WriteLine("\nOh so your name is " + yourName + "?\n");
                string yesOrNo = Console.ReadLine();
                if (yesOrNo.Contains("Yes") || yesOrNo.Contains("Y"))
                    isName = true;
                else
                {
                    Console.WriteLine();
                    yourName = getYourName();
                }
            }
            string faveLang = getFavoriteLanguage(yourName);
            if (isCSharp(faveLang))
                Console.WriteLine("\n" + yourName + "... C# is your favorite prog. language? Correctamundo!!!");
            else if (faveLang.Equals("Java"))
                Console.WriteLine("\nJava is alright, I guess. You're alright with me, " + yourName);
            else
                Console.WriteLine("\n" + faveLang + " is your favorite prog. language? You're crazy, " + yourName + "!");
        }
    }
}
